var amazing = angular.module('AmazingApp', ['ngMaterial','ui.router']);

amazing.config(function($stateProvider,$urlRouterProvider){
   var mainState = {
     name: "main",
     url: "/main",
     templateUrl: "views/main.html"
   };
   var loginState = {
     name: "login",
     url: "/login",
     templateUrl: "views/login.html"
   }

   $stateProvider.state(mainState);
   $stateProvider.state(loginState);


   $urlRouterProvider.otherwise("/main");
});

amazing.controller('MainController', function($scope){

  console.log("TESTING....");
  alert("Im working ooo");
  $scope.working = "Hellloooo";

});
